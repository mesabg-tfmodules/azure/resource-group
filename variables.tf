variable "name" {
  type        = string
  description = "Resource group name"
}

variable "location" {
  type        = string
  description = "Deployment region"
  default     = "eastus"
}

variable "tags" {
  type        = map
  description = "Additional default tags"
  default     = {}
}
