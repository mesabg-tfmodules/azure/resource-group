resource "azurerm_resource_group" "this" {
  name      = "rg-${var.name}"
  location  = var.location

  tags      = local.tags

  lifecycle {
    prevent_destroy = true
  }
}
