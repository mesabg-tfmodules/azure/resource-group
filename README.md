# Resource Group Module

This module is capable to create a single Resource Group.

Module Input Variables
----------------------

- `name` - Resource group name
- `location` - Resource group deployment region

Usage
-----

```hcl
module "resource_group" {
  source    = "git::https://gitlab.com/mesabg-tfmodules/azure/resource-group.git"

  name      = "my-resource-group"
  location  = "eastus"
}
```

Outputs
=======

 - `id` - Resource group identifier
 - `name` - Resource group name
 - `location` - Deployment region

Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
