locals {
  tags      = merge(var.tags, {
    source  = "terraform"
  })
}
